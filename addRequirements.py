import datetime

import csv
import firebase_admin
import google.cloud
from firebase_admin import credentials, firestore
import unidecode

cred = credentials.Certificate("ServiceAccountKey.json")
app = firebase_admin.initialize_app(cred)

store = firestore.client()


with open('requirements.csv') as weeks:
    weeks = csv.reader(weeks)
    for row in weeks:
        if (row[0] != "" and row[2] != "start_date"):
            start_date_string = ""
            for i, num in enumerate(row[2].split("/")):
                if i != 2:
                    if len(num) == 1:
                        value = "0" + num
                    else:
                        value = num
                else:
                    value = "20" + num
                start_date_string += value
                if len(value) != 4:
                    start_date_string += "/"

            end_date_string = ""
            for i, num in enumerate(row[3].split("/")):
                if i != 2:
                    if len(num) == 1:
                        value = "0" + num
                    else:
                        value = num
                else:
                    value = "20" + num
                end_date_string += value
                if len(value) != 4:
                    end_date_string += "/"
            start_date = datetime.datetime.strptime(start_date_string, "%m/%d/%Y")
            start_date = start_date.replace(hour=19, minute=1)
            end_date = datetime.datetime.strptime(end_date_string, "%m/%d/%Y")
            end_date = end_date.replace(hour=19)
            new_dict = {"interviews": row[1].decode('utf-8'), "start_date": start_date, "end_date": end_date}

            doc_ref = store.collection("Requirements").document(row[0].decode('utf-8'))
            doc_ref.set(new_dict)
