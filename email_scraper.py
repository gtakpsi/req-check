from __future__ import print_function
import pickle
import os.path
from googleapiclient.discovery import build, Resource
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request
import base64
from pprint import pprint

from googleapiclient.errors import HttpError
from pprint import pprint
import firebase_admin
from firebase_admin import credentials
from firebase_admin import firestore
from firebase_admin.firestore import client as firebaseClient
import dateutil.parser as parse_date
import dateutil.tz
import re

"""
This program scans the users inbox for pledge interview requests

Credential Creation:
    1. Step 1 at https://developers.google.com/gmail/api/quickstart/python
    2. Make sure to select Web Server
    3. Make sure the redirect URI has the ending slash. EX: http://localhost:5000/ works but http://localhost:5000 does not
    4. If you want to change the port of localhost make sure to change it in creds = flow.run_local_server(port=5000) as well

Understanding how gmail's API is slightly confusing because there isn't direct python documentation.
However, this resource is pretty helpful for understanding how to translate their documentation to python:
https://medium.com/better-programming/a-beginners-guide-to-the-google-gmail-api-and-its-documentation-c73495deff08

But basically users.messages.list becomes users().messages().list(userId=me, otherParams=likeThis).execute()

DOCS: https://developers.google.com/gmail/api/reference/rest
"""



class EmailScraper:


    def __init__(self, gmail_service: Resource = None, firebase_service: firebaseClient = None):
        self.gmail_service = self.initalize_Oauth() if gmail_service is None else gmail_service
        self.firebase_service = self.initialize_firebase() if firebase_service is None else firebase_service

    def initalize_Oauth(self):
        """Initializes Oauth if needed returns api service"""
        # If modifying these scopes, delete the file token.pickle.
        SCOPES = ['https://www.googleapis.com/auth/gmail.readonly']

        creds = None
        # The file token.pickle stores the user's access and refresh tokens, and is
        # created automatically when the authorization flow completes for the first
        # time.
        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                creds = pickle.load(token)
        # If there are no (valid) credentials available, let the user log in.
        if not creds or not creds.valid:
            if creds and creds.expired and creds.refresh_token:
                creds.refresh(Request())
            else:
                flow = InstalledAppFlow.from_client_secrets_file(
                    'credentials.json', SCOPES)
                creds = flow.run_local_server(port=5000)
            # Save the credentials for the next run
            with open('token.pickle', 'wb') as token:
                pickle.dump(creds, token)

        gmail_service = build('gmail', 'v1', credentials=creds)
        return gmail_service


    def get_emails(self, query=""):
        """
        Returns emails based on query
        query is gmail's standard query format: https://support.google.com/mail/answer/7190?hl=en
        """

        # Use the Gmail API to get messages specifed by the query
        results = self.gmail_service.users().messages().list(userId='me', q=query).execute()
        messageIDs = results.get('messages')

        if not messageIDs:
            print(f'No messages for "{query}" found.')
            messageIDs = []
        return messageIDs

    def format_email_content(self, emails: list):
        """
        Returns a dict where key = messageID, value = emailData
        {
            "messageID": {  "pledge_email": None,
                            "brother_email": None,
                            "date": None,
                            "email_type": None,
                            "content": None },
        }
        """

        messageData = {}
        for messageID in emails:
            emailData = {"pledge_email": None, "brother_email": None, "date": None, "email_type": None, "content": None }

            fullMessage = self.gmail_service.users().messages().get(userId='me', id=messageID.get('id')).execute()

            # Get email body
            try:
                b64MessageData = fullMessage.get('payload').get('parts')[0].get('body').get('data')
                message = base64.urlsafe_b64decode(b64MessageData).decode("UTF8")
                emailData['content'] = message
            except:
                print("Failed to find email body")

            # Get headers: pledge_email, brother_email, email_type, date
            try:
                headers = {}
                for header in fullMessage.get('payload').get('headers'):
                    headers[header['name']] = header['value']

                emailData['pledge_email'] = headers['From'].split("<")[1][:-1]
                emailData['brother_email'] = headers['To'].split("<")[1][:-1]
                emailData['date'] = parse_date.parse(headers["Date"]).astimezone(tz=dateutil.tz.UTC)

                #TODO: Determine if we should read the subject and then convert to type or just use the subject line
                #TODO: EX: "AKPsi Brother Interview Request" vs "Interview"
                emailData['email_type'] = headers['Subject']

            except:
                print("Failed to find one or more of the following: pledge_email, brother_email, subject, date")

            messageData[messageID.get('id')] = emailData
        return messageData

    def initialize_firebase(self):
        """returns firebase client"""
        cred = credentials.Certificate('ServiceAccount.json')
        app = firebase_admin.initialize_app(cred)
        db = firestore.client()
        return db


    def return_new_emails(self, emails: list):
        """Removes emails already in the database based on messageID"""
        dbemailIDs = [dbmsg.id for dbmsg in self.firebase_service.collection(u'Emails').stream()]
        new_emails = [message for message in emails if message.get('id') not in dbemailIDs]
        return new_emails




    def format_db_entry(self, emails: dict):
        """
        modifies all emails into this format:
        {week: int, pledge_id: str, email_hash: str, email_content: dict}
        """
        db_entries = []
        weeks = {week.id: week.to_dict() for week in self.firebase_service.collection(u'Requirements').stream()}
        for messageID, email_contents in emails.items():
            pledge_db_entry = self.firebase_service.collection("Pledges").where("email", "==", email_contents["pledge_email"]).stream()
            single_entry =  {
                "week": None,
                "pledge_id": next(pledge_db_entry).id,
                "email_hash": messageID,
                "email_content": email_contents
                }

            for week_num, week_data in weeks.items():
                if week_data["end_date"] > email_contents["date"]:
                    single_entry["week"] = week_num
                    break;

            db_entries.append(single_entry)
        return db_entries

def add_all_pledge_emails():
    """Example usage: Returns Thank You emails"""
    email_scraper = EmailScraper()
    messageIDs = email_scraper.get_emails("subject:Thank You")
    messageIDs = email_scraper.return_new_emails(messageIDs)
    emails = email_scraper.format_email_content(messageIDs)
    return email_scraper.format_db_entry(emails)


if __name__ == '__main__':
    r = add_all_pledge_emails()
    pprint(r)
