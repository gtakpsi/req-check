import csv
from req_check.DB import firebase


db = firebase()
with open('AKΨ Contact Log 2019-2020 - Brothers.csv', 'r') as f:
    reader = csv.reader(f, delimiter = ',')
    for ix, row in enumerate(reader):
        if ix == 0:
            continue

        brother_first = row[0]
        brother_last = row[1]
        brother_email = row[3]
        username = brother_email.replace('.', '').split('@')[0]

        db['brothers', username] = {
            'brother_id': brother_first + brother_last,
            'first_name': brother_first,
            'brother_last': brother_last,
            'brother_email': brother_email}