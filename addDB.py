import datetime

import firebase_admin
import google.cloud
from firebase_admin import credentials, firestore
import unidecode

cred = credentials.Certificate("ServiceAccountKey.json")
app = firebase_admin.initialize_app(cred)

store = firestore.client()

test_email_content = { "pledge_email": u"amandani3@gatech.edu",
                       "brother_email": u"vadini@gatech.edu",
                       "date": datetime.datetime.now(),
                       "email_type": u"ThankYou",
                       "content": u"Thank you for having such a great interview" }

def addToDB(week, pledge_id, email_hash, email_content):
    store.collection("Emails").document(email_hash).set(email_content)
    email_id = email_hash

    doc_ref = store.collection("Weeks").document("Weeks").collection(str(week)).document(pledge_id)
    data = doc_ref.get().to_dict()

    if email_content["email_type"] == "Thank You":
        if not data or "thank_you_emails" not in data:
            email_content_dict = {
                "thank_you_emails": [email_id]
            }
        else:
            email_ids = data["thank_you_emails"]
            email_ids.append(email_id)
            email_content_dict = {
                "thank_you_emails": email_ids
            }
        doc_ref.set(email_content_dict, merge=True)
    elif email_content["email_type"] == "BrotherEmail":
        if not data or "brother_emails" not in data:
            email_content_dict = {
                "brother_emails": [email_id.decode('utf-8')]
            }
        else:
            email_ids = data["brother_emails"]
            email_ids.append(email_id.decode('utf-8'))
            email_content_dict = {
                "brother_emails": email_ids
            }
        doc_ref.set(email_content_dict, merge=True)
    else:
        print("Something went wrong")

if __name__ == "__main__":
    addToDB(2, "Mandani", test_email_content)
