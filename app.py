from flask import Flask, request, jsonify, render_template,redirect, url_for, session
import firebase_admin
from firebase_admin import credentials, firestore
from email_scraper import EmailScraper
import addDB
from apscheduler.schedulers.background import BackgroundScheduler

app = Flask(__name__,template_folder='templates')
app.secret_key = "secreto"

def get_all_pledge_emails():
    """Example usage: Returns Thank You emails"""
    email_scraper = EmailScraper(firebase_service=addDB.store)
    messageIDs = email_scraper.get_emails("subject:Thank You")
    messageIDs = email_scraper.return_new_emails(messageIDs)
    emails = email_scraper.format_email_content(messageIDs)
    formated_emails = email_scraper.format_db_entry(emails)
    print("Reached")
    for email in formated_emails:
        print("Adding:", email["week"], email["pledge_id"], email["email_hash"])
        addDB.addToDB(email["week"], email["pledge_id"], email["email_hash"], email["email_content"])

class Database:
    def __init__(self):
        cred = credentials.Certificate("serviceAccount.json")
        app = firebase_admin.initialize_app(cred, name='Application')
        self.store = firestore.client()

    def getPledges(self):
        return [doc_ref.get().to_dict() for doc_ref in self.store.collection('Pledges').list_documents()]

    def getWeekEmailInfo(self, pledge):
        reqs = {}
        week_data = {}
        for doc_ref in self.store.collection('Requirements').list_documents():
            week = doc_ref._path[-1]
            reqs[week] = doc_ref.get().to_dict()
            week_data[week] = {'thank_you_emails': [], 'reqs': reqs[week]['interviews'], 'comp': 0}

        for collection_ref in self.store.collection('Weeks').document('Weeks').collections():
            week = collection_ref._path[-1]
            week_data[week] = {'thank_you_emails': [], 'reqs': reqs[week]['interviews'], 'comp': 0}
            for doc_ref in collection_ref.list_documents():
                temp_pledge = doc_ref._path[-1]
                if temp_pledge != pledge:
                    continue
                week_data[week]['thank_you_emails'] = doc_ref.get().to_dict()['thank_you_emails']

        for week, categories in week_data.items():
            for type, email_list in categories.items():
                if type != 'thank_you_emails':
                    continue
                for ix, email in enumerate(email_list):
                    email_collection = self.store.collection('Emails').document(email).get().to_dict()
                    if len(email_collection) == 0:
                        continue
                    week_data[week][type][ix] = email_collection
                    if week_data[week][type][ix]['date'] >= reqs[week]['start_date'] and week_data[week][type][ix]['date'] <= reqs[week]['end_date']:
                        week_data[week]['comp']+=1
        return week_data

    def getWeekSummaryData(self):
        pledges = self.getPledges()
        pledge_ids = {pledge['first_name'].lower() + '_' + pledge['last_name'].lower(): pledge for pledge in pledges}

        reqs = {}
        week_data = {}
        for doc_ref in self.store.collection('Requirements').list_documents():
            week = doc_ref._path[-1]
            reqs[week] = doc_ref.get().to_dict()
            week_data[week] = {'reqs': reqs[week]['interviews'], 'pledges': {pledge_ids[temp_pledge]['first_name'] + ' ' + pledge_ids[temp_pledge]['last_name']: 0 for temp_pledge in pledge_ids}}

        for collection_ref in self.store.collection('Weeks').document('Weeks').collections():
            week = collection_ref._path[-1]
            for doc_ref in collection_ref.list_documents():
                temp_pledge = doc_ref._path[-1]
                if temp_pledge not in pledge_ids:
                    continue
                week_data[week]['pledges'][pledge_ids[temp_pledge]['first_name'] + ' ' + pledge_ids[temp_pledge]['last_name']] += 1

        return week_data

db = Database()

@app.route('/')
def homepage():
    return render_template('all.html', data = db.getPledges(), pledge_info=db.getWeekSummaryData())

@app.route('/showPledge')
def pledge():
    pledge = request.args.get('pledge')
    print(pledge)
    return render_template('pledge.html', data=db.getPledges(), pledge_info=db.getWeekEmailInfo(pledge))

scheduler = BackgroundScheduler(timezone = 'America/New_York')
scheduler.add_job(func = get_all_pledge_emails, trigger = "cron", hour = "%s" % (23), minute = '30')
scheduler.start()