import csv
import firebase_admin
import google.cloud
from firebase_admin import credentials, firestore
import unidecode

cred = credentials.Certificate("serviceAccount.json")
app = firebase_admin.initialize_app(cred)

store = firestore.client()

ids = set()

with open('fall2020pledges.csv') as pledges:
    pledges = csv.reader(pledges, quotechar='|')
    for row in pledges:
        if (row[0] != ""):
            pledge_id = row[0] + "_" + row[1]
            if (row[1] not in ids):
                ids.add(row[0] + "_" + row[1])
            else:
                pledge_id = str(row[0] + "_" + row[1] + '1')

            new_dict = {"first_name": row[0].decode('utf-8'), "last_name": row[1].decode('utf-8'), "email": row[2].decode('utf-8'), "major": row[3].decode('utf-8')}
            print(new_dict)

            doc_ref = store.collection("Pledges").document(pledge_id.lower().decode('utf-8'))
            doc_ref.set(new_dict)
