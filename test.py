import firebase_admin
from firebase_admin import credentials, firestore
from pprint import pprint
from collections import defaultdict

class Database:
    def __init__(self):
        cred = credentials.Certificate("serviceAccount.json")
        app = firebase_admin.initialize_app(cred, name='app')
        self.store = firestore.client(app=app)

    def getPledges(self):
        return [doc_ref.get().to_dict() for doc_ref in self.store.collection('Pledges').list_documents()]

    def getWeekEmailInfo(self, pledge):
        reqs = {}
        week_data = {}
        for doc_ref in self.store.collection('Requirements').list_documents():
            week = doc_ref._path[-1]
            reqs[week] = doc_ref.get().to_dict()
            week_data[week] = {'thank_you_emails': [], 'reqs': reqs[week]['interviews'], 'comp': 0}

        for collection_ref in self.store.collection('Weeks').document('Weeks').collections():
            week = collection_ref._path[-1]
            for doc_ref in collection_ref.list_documents():
                temp_pledge = doc_ref._path[-1]
                if temp_pledge != pledge:
                    continue
                week_data[week]['thank_you_emails'] = doc_ref.get().to_dict()['thank_you_emails']

        for week, categories in week_data.items():
            for type, email_list in categories.items():
                if type != 'thank_you_emails':
                    continue
                for ix, email in enumerate(email_list):
                    email_collection = self.store.collection('Emails').document(email).get().to_dict()
                    if len(email_collection) == 0:
                        continue
                    week_data[week][type][ix] = email_collection
                    if week_data[week][type][ix]['date'] >= reqs[week]['start_date'] and week_data[week][type][ix]['date'] <= reqs[week]['end_date']:
                        week_data[week]['comp']+=1
        return week_data

    def getWeekSummaryData(self):
        pledges = self.getPledges()
        pledge_ids = {pledge['first_name'].lower() + '_' + pledge['last_name'].lower(): pledge for pledge in pledges}

        reqs = {}
        week_data = {}
        for doc_ref in self.store.collection('Requirements').list_documents():
            week = doc_ref._path[-1]
            reqs[week] = doc_ref.get().to_dict()
            week_data[week] = {'reqs': reqs[week]['interviews'], 'pledges': {pledge_ids[temp_pledge]['first_name'] + ' ' + pledge_ids[temp_pledge]['last_name']: 0 for temp_pledge in pledge_ids}}

        for collection_ref in self.store.collection('Weeks').document('Weeks').collections():
            week = collection_ref._path[-1]
            for doc_ref in collection_ref.list_documents():
                temp_pledge = doc_ref._path[-1]
                if temp_pledge not in pledge_ids:
                    continue
                week_data[week]['pledges'][pledge_ids[temp_pledge]['first_name'] + ' ' + pledge_ids[temp_pledge]['last_name']] += 1

        return week_data

db = Database()
db.getWeekSummaryData()